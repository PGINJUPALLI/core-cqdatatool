#!/bin/bash

## Specify the location where your input is stored
working_loc="/tmp"

## Specify the name of input file
input_file="$working_loc/input_movenodes.txt"

## How many instances you want to run your java app
num_partitions=$1

if [ -z "$1" ]; then
echo "Number of instances not specified,so defaulting to 1"
num_partitions=1;
fi

## Placeholder file, this is a temp file
curr_file="$working_loc/input_part.txt"

## To determine number of rows for a partition
total_rows=$(cat "$input_file" | wc -l)
part_size=$(( total_rows / num_partitions ))
echo "PartSize:$part_size"

## Delete files that got created from previous runs
temp_parts="$working_loc"/input_part*.txt
if [ -s "$temp_parts" ]; then
rm "$temp_parts"
fi

## Remove all older cronjobs related to this
crontab -l | grep -v '/home/ec2-user/core-cqdatatool/target/CQDataTool-1.0.one-jar.jar' | crontab -

###################################################################################################
## Amazon s3 related
## This downloads the input file to working loc
#outputFile="$input_file"
#amzFile="https://s3.amazonaws.com/core-datamigration/sampleinput.txt"
#bucket="core-datamigration"
#resource="/${bucket}/${amzFile}"
#contentType="text/plain"
#dateValue=`date -R`
#stringToSign="GET\n\n${contentType}\n${dateValue}\n${resource}"
#s3Key="AKIAJWU4WXVBOAY74DCQ"
#s3Secret="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"
#signature=`echo -en ${stringToSign} | openssl sha1 -hmac ${s3Secret} -binary | base64`
#curl  -H "Host: ${bucket}.s3.amazonaws.com" \
#     -H "Date: ${dateValue}" \
#     -H "Content-Type: ${contentType}" \
#     -H "Authorization: AWS ${s3Key}:${signature}" \
#     https://${bucket}.s3.amazonaws.com/${amzFile} -o $outputFile
###################################################################################################

x=1
count=1
java_cmd=""
tempcron="$working_loc/tempcron.txt"
time_incr=90


while read line
do
echo $line >> "$curr_file"
if [ $count -eq $part_size ]; then
cat $curr_file > "$working_loc/input_part_$x.txt"
rm $curr_file
####
# java logic goes here
java_cmd="java -Dinfile=$working_loc/input_part_$x.txt -DconfigFile=cqdoconfig.xml -jar /home/ec2-user/core-cqdatatool/target/CQDataTool-1.0.one-jar.jar > /tmp/cqdatatool_$x.log"
####
## #write out current crontab
crontab -l > mycron
##echo new cron into cron file
hourx=$(date --date "+$time_incr min" "+%H")
minutesx=$(date --date "+$time_incr min" "+%M")
datex=$(date --date "+$time_incr min" "+%d")
echo "Minutes:$minutesx and Hour:$hourx"
echo "$minutesx $hourx $datex * * $java_cmd" >> mycron
#install new cron file
cat mycron
crontab mycron
time_incr=$((time_incr+90))
x=$((x+1))
count=1
fi
count=$((count+1))
done < $input_file

if [ -s "$curr_file" ] || [ -s mycron ]
then
rm "$curr_file"
rm mycron
fi
echo "program completed"

