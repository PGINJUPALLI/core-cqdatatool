package test.java.com.sni.home.CQDataTool;

import org.apache.commons.lang3.ArrayUtils;
import java.io.File;

/**
 * Static methods and useful strings
 * @author Pavan Ginjupalli
 *         Date: 05/29/14
 */
public class Util {

    public static final String EMPTY_STRING = "";
    public static final String SPACE = " ";
    public static final String DELIMITER = ">";
    public static final File inputfile=new File("C:/files/outputfiles/input.txt");
    public static final File outputfile=new File("C:/files/outputfiles/output.txt");
    /**
     * use this to format an exception message
     * @param message String message to prepend to exception info
     * @param e Exception
     * @return String formatted exception message
     */
    public static String formatExceptionMessage(final String message, final Exception e) {
        if (message != null && e != null) {
            StringBuilder builder = new StringBuilder();
            builder
                    .append(message).append(SPACE)
                    .append(e.getMessage()).append(SPACE)
                    .append(ArrayUtils.toString(e.getStackTrace()));
            return builder.toString();
        } else {
            return EMPTY_STRING;
        }
    }

    /**
     * Enum: Queries
     * Queries that could be used to find nodes to process
     */
    public static enum Queries {
        IMAGES_SQL2 ("select * from [nt:base] AS s where ISDESCENDANTNODE([/content/dam/images/hgtv/BUCKET/YEAR]) AND ([dc:copyright] is not null  OR [sni:copyright] is not null  OR  [sni:dc-copyright] is not null)");
        //select * from nt:base where jcr:path like '/content/dam/images/hgtv/fullset/%' and dc:copyright is not null
        private String query;

        private Queries(final String query) {
            this.query = query;
        }

        public String query() {
            return query;
        }
    }

    /**
     * Enum: HostSettings
     * yep
     */
    public static enum HostSettings {

        LOCALHOST ("http://localhost:4502", "admin", "admin", "/content/dam"),
        DEV ("http://author.hgtv-dev.sni.hgtv.com", "admin", "admin", "/content/dam/images/hgtv");//,
        //QA ("http://cctvauthqa.scrippsnetworks.com", "admin", "admin", "/content/cook"),
        //STAGING ("http://cctvauthstage.scrippsnetworks.com", "admin", "admin", "/content/cook"),
        //PROD ("http://cctvauthprod.scrippsnetworks.com", "admin", "admin", "/content/cook");

        private String host;
        private String user;
        private String pass;
        private String startingPath;
        private String[] startingPaths;

        private HostSettings(final String host, final String user, final String pass, final String path) {
            this.host = host;
            this.user = user;
            this.pass = pass;
            this.startingPath = path;
        }

        private HostSettings(final String host, final String user, final String pass, final String[] paths) {
            this.host = host;
            this.user = user;
            this.pass = pass;
            this.startingPaths = paths;
            this.startingPath = paths[0];
        }

        public String host() { return this.host; }
        public String user() { return this.user; }
        public String pass() { return this.pass; }
        public String startingPath() { return this.startingPath; }
        public String[] startingPaths() { return this.startingPaths; }
    }
}
