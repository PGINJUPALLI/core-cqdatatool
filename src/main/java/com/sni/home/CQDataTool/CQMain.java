package com.sni.home.CQDataTool;

import com.sni.home.CQDataTool.config.CQDataConfig;
import com.sni.home.CQDataTool.interfaces.CQDataObject;
import com.sni.home.CQDataTool.interfaces.Connection;
import com.sni.home.CQDataTool.models.CQConnectionImpl;
import com.sni.home.CQDataTool.models.CQDataFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * @author Pavan Ginjupalli
 */
public class CQMain {

	static Logger log = LoggerFactory.getLogger(CQMain.class);

	/**

     * To run this tool
     You need to use maven for build

     - The main configuration is in cqdoconfig.xml
     - You need to change parameters in Bean "BASECONFIG"

     java -Xmx1G -Dinfile=C:\files\outputfiles\sampleinput.txt -Doutfile=C:\files\outputfiles\output.txt -DconfigFile=cqdoconfig.xml -jar ./target/CQDataTool-1.0.one-jar.jar
     */

	public static void main(final String[] args) {

        long startTime = System.currentTimeMillis();
        DateFormat df=new SimpleDateFormat("hh:mm:ss:SSS");
        String program_start_date = new Date().toString();
		log.info("starting application on "+ program_start_date);
		Connection connection = null;
		try {
			connection = new CQConnectionImpl();
			connection.login();
            CQDataConfig cqdoconf = new CQDataConfig();
			cqdoconf.setRootNode(connection.getNode());
            cqdoconf.setSession(cqdoconf.getRootNode().getSession());
            ConfigurableApplicationContext context = new ClassPathXmlApplicationContext(System.getProperty("configFile"));
            CQDataConfig cqdataconf = (CQDataConfig) context.getBean("BASECONFIG");
			log.info("operation type:"+cqdataconf.getOperationtype());
			log.info("bean name:"+cqdataconf.getBeanname());
			CQDataObject cqdo = CQDataFactory.getCQObject(cqdataconf.getOperationtype());
			cqdo.processCQObject(cqdataconf.getBeanname());
			connection.logout();
            long endTime   = System.currentTimeMillis();
            long millis = endTime - startTime;
            String totalTime = String.format("%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(millis),
                    TimeUnit.MILLISECONDS.toMinutes(millis) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)),
                    TimeUnit.MILLISECONDS.toSeconds(millis) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
            log.info("It took "+totalTime+" [hours:minutes:seconds] to finish this program");
            log.info("Application started on: "+program_start_date+" and Application exiting under normal conditions on "+new Date().toString());
			System.exit(0);
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(1);
		}
	} //End of Main
} //End of class CQMain