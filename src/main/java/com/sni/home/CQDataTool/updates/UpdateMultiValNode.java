package com.sni.home.CQDataTool.updates;

import java.io.BufferedReader;
import java.io.FileReader;
import javax.jcr.Node;
import javax.jcr.Property;
import javax.jcr.PropertyIterator;
import com.sni.home.CQDataTool.config.CQDataConfig;
import com.sni.home.CQDataTool.config.ExceptionMsg;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

	/**
	 * @author Pavan Ginjupalli
	 */
public class UpdateMultiValNode { 

	    private static final Logger log = LoggerFactory.getLogger(UpdateMultiValNode.class);

	    public static void updateProperty(CQDataConfig cfg) throws Exception {
	    	
	    	

	        try {
	        	System.out.println("The Input update file name is: "+cfg.getInfile());
	            FileReader input = new FileReader(cfg.getInfile());
	            BufferedReader bufRead = new BufferedReader(input);
	            String myLine = null;
	            int saverecordno=0;
	            int bufrecordno=0;
	            Node cqNode = null;
	            
	            

	            while ((myLine = bufRead.readLine()) != null)
	            {
	                String[] param = myLine.split(cfg.getDelimit(),-1);
	                for(int i=0;i<param.length;i++)
	                {
	                   // log.info("Arg "+i+" is :"+param[i].toString()+"\n");
	                }

	                String cqNodeStr 		= param[0].toString();	
	                int item=1;
	                
	                try
	                {
	                cqNode = CQDataConfig.getRootNode().getSession().getNode(cqNodeStr);
	                
	                }
	                catch (Exception notanode)
	                {
	                	System.out.println(cqNodeStr+" does not exist..skipping to next");
	                }
	                
	                System.out.println("\n"+saverecordno+"/"+bufrecordno+": Node String getting updated is:"+cqNodeStr);
	                
	                for (String lfield : cfg.getFields()) 
					{
	                	
	                	PropertyIterator propIterator = cqNode.getProperties();
	                
			                while (propIterator.hasNext())
			                {
			                    Property prop = propIterator.nextProperty();
			                    
			                    if (prop.getName().equals(lfield) && !param[item].toString().isEmpty()) 
			                    {	 
			                    	      String[] multval=new String[1];
				                          multval[0]=param[item];
			                    	
		    	                          try{
		    	                          cqNode.setProperty(prop.getName(),multval);  
		    	                          }
		    	                          catch(Exception e){
		    	                        	  System.out.println("Your exception is:"+e);
		    	                          }   
			                    }  
			                }// end while loop
			              item++;	                
					}// end for loop getFields
	                
	   
	                
	                if(saverecordno>50)
	                {
	                	saverecordno=0;
	                	CQDataConfig.getRootNode().getSession().save();
	                	log.info("*********** Saving the updates **********************");
	                }else{
	                	saverecordno=saverecordno+1;
	                }
	                
	                bufrecordno=bufrecordno+1;

	            }
	            CQDataConfig.getRootNode().getSession().save();
	            bufRead.close();
	        }
	        catch (Exception e)
	        {
	            log.error(ExceptionMsg.formatExceptionMessage("Run Update Exception", e));
	        }

	    }//End updateProperty method

}
