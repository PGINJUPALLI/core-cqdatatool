package com.sni.home.CQDataTool.updates;

import com.sni.home.CQDataTool.config.CQDataConfig;
import com.sni.home.CQDataTool.utils.PropertyExists;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jcr.Property;
import javax.jcr.RepositoryException;
import javax.jcr.Session;

/**
 * Created by 161257 on 6/1/2016.
 */
public class UpdateProperty {

    private static final Logger log = LoggerFactory.getLogger(UpdateProperty.class);

    private static Session s;

    public static String updateProperty(String nodestr,String propertyname,String propertyvalue,CQDataConfig cfg,Boolean overwrite) throws RepositoryException
    {
        s = cfg.getSession();

            try {
                if (!PropertyExists.propertyExists(s.getNode(nodestr), propertyname) && !overwrite) {
                    s.getNode(nodestr).setProperty(propertyname, propertyvalue);
                }else if(overwrite)
                {
                    s.getNode(nodestr).setProperty(propertyname, propertyvalue);
                }
            }
            catch (Exception e) {
                log.info("Your exception is:" + e);
            }

        return null;

    }
}
