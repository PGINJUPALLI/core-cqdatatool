package com.sni.home.CQDataTool.utils;

import com.sni.home.CQDataTool.config.CQDataConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jcr.Session;
import javax.json.*;
import java.io.InputStream;
import java.net.Authenticator;
import java.net.HttpURLConnection;
import java.net.PasswordAuthentication;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by 161257 on 6/19/2016.
 */
public class GetJSONvalues {

    private static final Logger log = LoggerFactory.getLogger(GetJSONvalues.class);
    private static String username = null;
    private static String password = null;
    private static Session s;


    public static ArrayList < String > getJSONvalues(String inurl, String ParentName, String KeyName, CQDataConfig cfg) throws Exception {
        ArrayList < String > KeyVals = new ArrayList < > ();
        username = cfg.getUser();
        password = cfg.getPassword();
        s = cfg.getSession();
        try {
            URL url = new URL(inurl);
               Authenticator.setDefault(new Authenticator() {
                   @Override
                   protected PasswordAuthentication getPasswordAuthentication() {
                       return new PasswordAuthentication(username, password.toCharArray());
                   }
               });

            //String creds = javax.xml.bind.DatatypeConverter.printBase64Binary(new String(username + ":" + password).getBytes(Charset.forName("UTF-8")));

            HttpURLConnection conn = (HttpURLConnection)url.openConnection();
            //conn.setRequestProperty("Authorization", "Basic " + creds);
            InputStream is = conn.getInputStream();
            JsonReader rdr = Json.createReader(is);
            JsonObject obj = rdr.readObject();
            JsonArray results = obj.getJsonArray(ParentName);
            for (JsonObject result: results.getValuesAs(JsonObject.class)) {
                for (JsonValue ref: result.getJsonArray(KeyName)) {
                    if (ref.toString().matches("(.*)jcr:content/sni:recipe(.*)")) {
                        String refStr = ref.toString().replaceAll("/jcr:content/sni:recipe[s]?", "");
                        log.info("Found a reference:" + refStr);
                        KeyVals.add(refStr);
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return KeyVals;
    }

}