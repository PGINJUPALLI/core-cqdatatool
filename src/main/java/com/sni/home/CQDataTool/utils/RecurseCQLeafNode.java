package com.sni.home.CQDataTool.utils;
import com.sni.home.CQDataTool.config.CQDataConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.RepositoryException;
import java.io.FileWriter;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

public class RecurseCQLeafNode {
	
    static String leafNode="";
    static String ln;
    static Logger log = LoggerFactory.getLogger(RecurseCQLeafNode.class);
    private static int i=0;
    private static long total=0;

	public static Boolean recursechildren(Node n, CQDataConfig cfg,FileWriter fw) throws RepositoryException, IOException
    {

      for (NodeIterator nia = n.getNodes(); nia.hasNext();)
		{
			Node na = nia.nextNode();

            if(na.isNodeType("cq:Page"))
            {
                leafNode=na.getPath().toString();
            }
		
			if (!na.hasNodes() )
			{
                ln = leafNode!= null ? leafNode : "";
                if( i == 1000 )
                {
                    System.out.println("Processed:"+"["+total+"] nodes so far...current processing node:" + ln);
                    i=0;
                }
                existsInAuthor(leafNode,cfg,fw);
                i=i+1;
                total=total+1;
			}

			else 
			{
				recursechildren(na, cfg,fw);
			}
		}

		return true;
	}
    public static  Boolean existsInAuthor(String nodestr,CQDataConfig cfg,FileWriter fw) throws RepositoryException, IOException
    {
        String fullpath=cfg.getUrlstr();
        if(nodestr!="")
        {
            fullpath = fullpath + nodestr + ".html";
        }

            try
            {
                URL inurl = new URL(fullpath);
                HttpURLConnection http = (HttpURLConnection)inurl.openConnection();
                String userpass = cfg.getUser() + ":" + cfg.getPassword();
                String basicAuth = "Basic " + javax.xml.bind.DatatypeConverter.printBase64Binary(userpass.getBytes());
                http.setRequestProperty("Authorization", basicAuth);
                int statusCode = http.getResponseCode();
                if(statusCode==200)
                {
                    return true;
                }
                else {
                    System.out.println("path:["+fullpath+"] responded with a status code :"+statusCode);
                    fw.append(fullpath+"\n");
                    return false;
                }
            }
            catch(Exception e)
            {
                System.out.println("Error:"+e);
                return false;
            }

    }

}
