package com.sni.home.CQDataTool.utils;

/**
 * Created by 161257 on 5/24/2016.
 */
import java.security.MessageDigest;
public class GetMD5Hash {


        public String create(String instr)throws Exception
        {

            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(instr.getBytes());

            byte byteData[] = md.digest();

            //convert the byte to hex format method 1
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < byteData.length; i++) {
                sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
            }

            System.out.println("Digest(in hex format):: " + sb.toString());

            //convert the byte to hex format method 2
            StringBuffer hexString = new StringBuffer();
            for (int i=0;i<byteData.length;i++) {
                String hex=Integer.toHexString(0xff & byteData[i]);
                if(hex.length()==1) hexString.append('0');
                hexString.append(hex);
            }
            System.out.println("InputString:"+instr+" in hex format:: " + hexString.toString());
            return hexString.toString();
        }

}

