package com.sni.home.CQDataTool.utils;

import com.sni.home.CQDataTool.config.CQDataConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.RepositoryException;
import java.io.FileWriter;
import java.io.IOException;

public class RecurseChildren {

    static String leafNode="";
    static String ln;
    static Logger log = LoggerFactory.getLogger(RecurseCQLeafNode.class);
    private static int i=0;
    private static long total=0;

    public static Boolean recursechildren(Node n, CQDataConfig cfg,FileWriter fw) throws RepositoryException, IOException
    {

        for (NodeIterator nia = n.getNodes(); nia.hasNext();)
        {
            Node na = nia.nextNode();

            if(na.isNodeType("cq:Page"))
            {
                leafNode=na.getPath().toString();
                fw.append(leafNode+"\n");
            }

            if (!na.hasNodes() )
            {
                ln = leafNode!= null ? leafNode : "";
                if( i == 1000 )
                {
                    System.out.println("Processed:"+"["+total+"] nodes so far...current processing node:" + ln);
                    i=0;
                }
                i=i+1;
                total=total+1;
            }

            else
            {
                recursechildren(na, cfg,fw);
            }
        }

        return true;
    }


}
