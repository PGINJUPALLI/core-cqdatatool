package com.sni.home.CQDataTool.utils;

import java.io.*;
import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sni.home.CQDataTool.config.ExceptionMsg;
import com.sni.home.CQDataTool.config.CQDataConfig;

public class FileOperations {

static Logger log	= LoggerFactory.getLogger(FileOperations.class);

static FileWriter fw;
	
	public static void appendtoFile(CQDataConfig cfg,ArrayList<String> filecontent) throws IOException 
	{
	
		try {
			
			     fw= new FileWriter(cfg.getOutfile());
			     
				 log.info(cfg.getOutfile());			
				 
				 fw.append(cfg.getColumns()+"\n");
				 
				 for(String data:filecontent)
				 	{
					 //System.out.println("filecontent:"+data);
					 fw.append(data+"\n");					 
					}
					fw.flush();
					fw.close();
			 }

		catch (Exception e) {
			log.error(ExceptionMsg.formatExceptionMessage(
					"File to write not found Exception", e));
			}
		

	}
    public static int countLines(String filepathStr) throws IOException {
        LineNumberReader reader = null;
        try {
            reader = new LineNumberReader(new FileReader(filepathStr));
            while ((reader.readLine()) != null);
            return reader.getLineNumber();
        } catch (Exception ex) {
            ex.printStackTrace();
            return -1;
        }finally {
            if(reader != null)
                reader.close();
        }
    }

    public static int countLinesB(String filename) throws IOException {
        InputStream is = new BufferedInputStream(new FileInputStream(filename));
        try {
            byte[] c = new byte[1024];
            int count = 0;
            int readChars = 0;
            boolean empty = true;
            while ((readChars = is.read(c)) != -1) {
                empty = false;
                for (int i = 0; i < readChars; ++i) {
                    if (c[i] == '\n') {
                        ++count;
                    }
                }
            }
            return (count == 0 && !empty) ? 1 : count;
        } finally {
            is.close();
        }
    }
	

	

}
