package com.sni.home.CQDataTool.utils;

import com.sni.home.CQDataTool.config.CQDataConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.query.Query;
import javax.jcr.query.QueryManager;
import javax.jcr.query.QueryResult;
import javax.jcr.query.RowIterator;

/**
 * Created by 161257 on 6/6/2016.
 */
public class QueryResults {

    private static final Logger log = LoggerFactory.getLogger(QueryResults.class);
    private static Session s;
    private static QueryResult result;

    public static RowIterator getQueryResults(CQDataConfig cfg,String queryStr)throws Exception,RepositoryException {
        try {
            s = cfg.getSession();
            QueryManager queryManager = s.getWorkspace().getQueryManager();
            Query q = queryManager.createQuery(queryStr, Query.JCR_SQL2);
            log.info("Executing query: " + queryStr);
            result = q.execute();

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return result.getRows();
    }
}
