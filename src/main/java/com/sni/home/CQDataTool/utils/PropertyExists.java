package com.sni.home.CQDataTool.utils;

import javax.jcr.Node;
import javax.jcr.Property;
import javax.jcr.PropertyIterator;
import javax.jcr.RepositoryException;

/**
 * Created by 161257 on 6/1/2016.
 */
public class PropertyExists {

    public static boolean propertyExists(Node cqNode, String propname)

    {
        try {

            PropertyIterator propIterator = cqNode.getProperties();

            int flag = 0;

            while (propIterator.hasNext()) {

                Property prop = propIterator.nextProperty();

                if (prop.getName().equals(propname)) {
                    flag = 1;
                }


            }

            if (flag == 1) {
                return true;
            } else {
                return false;
            }

        } catch (RepositoryException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return false;
        }



    } //End propertyExists Method
}
