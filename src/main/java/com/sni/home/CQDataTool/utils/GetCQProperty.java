package com.sni.home.CQDataTool.utils;

import java.util.HashMap;
import javax.jcr.Node;
import javax.jcr.Property;
import javax.jcr.PropertyIterator;
import javax.jcr.RepositoryException;

import com.sni.home.CQDataTool.config.CQDataConfig;

public class GetCQProperty {
	
	public HashMap<String,String> getProperty(Node n,CQDataConfig cfg) throws Exception{
		
		GetCQPropertyValue cqp = new GetCQPropertyValue();
		
		HashMap<String,String> al = new HashMap<String,String>();
		
		HashMap<String, Property> map = new HashMap<String, Property>();
		
		try {
			
			PropertyIterator pi = n.getProperties();
			
			while (pi.hasNext()) 
			{
				Property p = pi.nextProperty();
					
				map.put(p.getName().toString(), p);
						
			}
				
			for(String field:cfg.getFields())
			{
				
				for (String key : map.keySet()) 
				{
						
					if(field.equalsIgnoreCase(key))
						
						{						    
							
						//System.out.println("Property:"+key.toString());
						
						al.put(key.toString(),cqp.getCQPropertyValue(map.get(key)));
						    
						}
				}
			}

			return al;
			
		} catch (RepositoryException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
		 
				
	}

}
