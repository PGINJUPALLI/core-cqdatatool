package com.sni.home.CQDataTool.utils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.RepositoryException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sni.home.CQDataTool.config.CQDataConfig;

public class RecurseCQ {	
	
static Set<Node> vnodes= new HashSet<Node>();

static Logger	log	= LoggerFactory.getLogger(RecurseCQ.class);
		   
	public static Set<Node> recursechildren(Node n, CQDataConfig cfg) throws RepositoryException, IOException {

		for (NodeIterator nia = n.getNodes(); nia.hasNext();) 
		{
			
			Node na = nia.nextNode();
			
			vnodes.add(na.getParent());
		
			if (!na.hasNodes()) 
			{
				//log.info(na.getPath().toString()+" has no child nodes");
				
			}

			else 
			{
				recursechildren(na, cfg);
			}
		}
		return vnodes;

	}

}
