package com.sni.home.CQDataTool.interfaces;

import javax.jcr.Node;

/**
 * Created with IntelliJ IDEA.
 * User: Pavan Ginjupalli
 * Date: 2/2/16
 * Time: 9:12 PM
 * Returns a recipe template based on a type requested
 */
public interface Connection {
    public Node getNode();
    public void login() throws Exception;
    public void logout();
}
