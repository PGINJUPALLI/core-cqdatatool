package com.sni.home.CQDataTool.delete;

import com.sni.home.CQDataTool.config.CQDataConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jcr.Node;
import javax.jcr.PathNotFoundException;
import java.io.BufferedReader;
import java.io.FileReader;

/**
 * Created by 161257 on 6/22/2016.
 */
public class BulkDeleteNodes {
    private static final Logger log = LoggerFactory.getLogger(BulkDeleteNodes.class);

    public static void bulkDelete(CQDataConfig cfg) throws Exception {
int saveCount=1000;
        int counter=0;

        try {

            log.info("The file name is: "+cfg.getInfile());

            FileReader input = new FileReader(cfg.getInfile());

            BufferedReader bufRead = new BufferedReader(input);

            String myLine = null;

            while ((myLine = bufRead.readLine()) != null) {
                counter++;

                    String NodeStr = myLine.toString();

                    log.info("Deleting the node: " + NodeStr);

                    try {

                        Node n = CQDataConfig.getRootNode().getSession().getNode(NodeStr);

                        if (!n.equals(null)) {

                            n.remove();
                            if (counter == saveCount) {
                            CQDataConfig.getRootNode().getSession().save();
                            }
                        }
                    } catch (PathNotFoundException p) {
                        log.info("Not found, Skipping =====>Node " + NodeStr);
                    }


            }
            bufRead.close();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

    }

}
