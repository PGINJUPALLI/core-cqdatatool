package com.sni.home.CQDataTool.move;

import com.sni.home.CQDataTool.config.CQDataConfig;
import org.apache.jackrabbit.commons.JcrUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.Session;

/**
 * Created by 161257 on 6/1/2016.
 */
public class CopyNode {

    private static final Logger log = LoggerFactory.getLogger(CopyNode.class);

    private static Session s;

    public static String docopy(String sourcepath, String destinationpath, String newnodename,CQDataConfig cfg)
            throws RepositoryException {

        try {
            s = cfg.getSession();
            s.getNode(destinationpath);
        } catch (Exception ItemNotFound) {
            JcrUtils.getOrCreateUniqueByPath(destinationpath, "sling:Folder", s);
            s.save();
        }

        s.getWorkspace().copy(sourcepath, destinationpath + "/" + newnodename);
        s.save();

        return null;
    }
}
