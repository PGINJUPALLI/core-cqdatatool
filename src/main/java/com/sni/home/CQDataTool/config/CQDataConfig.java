package com.sni.home.CQDataTool.config;

import org.apache.sling.api.resource.ValueMap;

import javax.jcr.Node;
import javax.jcr.Repository;
import javax.jcr.Session;
import java.io.File;
import java.util.HashMap;
import java.util.Map;

public class CQDataConfig {
	
	private static Node rootNode;
    private HashMap<String,String> contentNodeProperties;
    private String bufferLine;
	private String updatetype;
    private String operationtype;
    private String beanname;
	private String outfile;
	private String infile;
	private String[] bucket;
	private String[] years;
	private String columns;
	private String delimit;
	private String query;
	private String[] fields;
    private String pkgpath;
    private String urlstr;
    private String host;
    private String path;
    private String user;
    private String password;
    private Repository repository;
    private static Session session;
    private Map propertymap;

    public Session getSession() {return session;}

    public void setSession(Session session) {this.session = session;}

    public String getOperationtype() {return operationtype;}

    public void setOperationtype(String operationtype) { this.operationtype = operationtype; }

    public String getBeanname() {return beanname;}

    public void setBeanname(String beanname) {this.beanname = beanname; }

    public String getUrlstr() {
        return urlstr;
    }

    public void setUrlstr(String urlstr) {
        this.urlstr = urlstr;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

	public static  Node getRootNode() {
		return rootNode;
	}

	public void setRootNode(Node rootNode) {
		CQDataConfig.rootNode = rootNode;
	}
	
	public String[] getBucket() {
		return bucket;
	}

	public void setBucket(String[] bucket) {
		this.bucket = bucket;
	}

	public String getOutfile() {
		return outfile;
	}

	public void setOutfile(String outfile) {
		this.outfile = outfile;
	}

	public String[] getYears() {
		return years;
	}

	public void setYears(String[] years) {
		this.years = years;
	}

	public String getColumns() {
		return columns;
	}

	public void setColumns(String columns) {
		this.columns = columns;
	}

	public String getDelimit() {
		return delimit;
	}

	public void setDelimit(String delimit) {
		this.delimit = delimit;
	}

	public String getUpdatetype() {
		return updatetype;
	}

	public void setUpdatetype(String updatetype) {
		this.updatetype = updatetype;
	}

	public String getInfile() {
		return infile;
	}

	public void setInfile(String infile) {
		this.infile = infile;
	}

	public String getQuery() {
		return query;
	}

	public void setQuery(String query) {
		this.query = query;
	}

	public String[] getFields() {
		return fields;
	}

	public void setFields(String[] fields) {this.fields = fields;	}

    public HashMap<String,String> getContentNodeProperties() {return contentNodeProperties;}

    public void setContentNodeProperties(HashMap<String,String> contentNodeProperties) {this.contentNodeProperties = contentNodeProperties; }

    public String getPkgpath() {
        return pkgpath;
    }

    public void setPkgpath(String pkgpath) {
        this.pkgpath = pkgpath;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public Repository getRepository() {
        return repository;
    }

    public void setRepository(Repository repository) {
        this.repository = repository;
    }

    public String getBufferLine() {return bufferLine; }

    public void setBufferLine(String bufferLine) { this.bufferLine = bufferLine; }

    public Map getPropertymap() {return propertymap;}

    public void setPropertymap(Map propertymap) { this.propertymap = propertymap;}



}
