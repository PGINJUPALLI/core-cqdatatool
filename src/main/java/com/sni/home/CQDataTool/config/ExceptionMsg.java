package com.sni.home.CQDataTool.config;

import org.apache.commons.lang3.ArrayUtils;

/**
 * Static methods and useful strings
 * @author Pavan Ginjupalli
 *         Date: 05/29/14
 */
public class ExceptionMsg {

    public static final String EMPTY_STRING = "";
    public static final String SPACE = " ";
    /**
     * use this to format an exception message
     * @param message String message to prepend to exception info
     * @param e Exception
     * @return String formatted exception message
     */
    public static String formatExceptionMessage(final String message, final Exception e) {
        if (message != null && e != null) {
            StringBuilder builder = new StringBuilder();
            builder
                    .append(message).append(SPACE)
                    .append(e.getMessage()).append(SPACE)
                    .append(ArrayUtils.toString(e.getStackTrace()));
            return builder.toString();
        } else {
            return EMPTY_STRING;
        }
    }
  
    
}
