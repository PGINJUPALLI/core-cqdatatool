package com.sni.home.CQDataTool.reports;

/**
 * Created by 161257 on 4/25/2016.
 */

import com.sni.home.CQDataTool.config.CQDataConfig;
import com.sni.home.CQDataTool.config.ExceptionMsg;
import com.sni.home.CQDataTool.utils.RecurseChildren;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jcr.Node;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.Date;

/**
 * @author Pavan Ginjupalli Date: 2/22/16
 */
public class TraverseAllChildren {

    private static final Logger log = LoggerFactory.getLogger(TraverseAllChildren.class);

    public static void getLeafNode(CQDataConfig cfg) throws Exception {

        String myLine;
        int i=0;
        Date start_dt,end_dt;

        try {

            start_dt=new Date();

            log.info("The file name is: "+cfg.getInfile());

            FileReader input = new FileReader(cfg.getInfile());

            BufferedReader bufRead = new BufferedReader(input);

            FileWriter fw = new FileWriter(cfg.getOutfile());

            fw.append("Process started at:"+start_dt+"\n");

            while ((myLine = bufRead.readLine()) != null)
            {

                String NodeStr 	= myLine.toString();

                log.info(i+":Processing the node: "+NodeStr);

                try {
                    Node n = CQDataConfig.getRootNode().getSession().getNode(NodeStr);

                    RecurseChildren.recursechildren(n, cfg, fw);


                }
                catch(Exception PathNotFoundException){
                    System.out.println("Error:"+PathNotFoundException);
                }

                i=i+1;


            }
            bufRead.close();
            end_dt= new Date();
            fw.append("Process completed at:"+end_dt+"\n");
            fw.close();
        }
        catch (Exception e)
        {
            System.out.println(e);
            log.error(ExceptionMsg.formatExceptionMessage("Run node based report Exception", e));
        }

    }





}

