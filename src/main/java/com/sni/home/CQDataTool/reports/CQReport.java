package com.sni.home.CQDataTool.reports;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.query.Query;
import javax.jcr.query.QueryResult;
import javax.jcr.query.Row;
import javax.jcr.query.RowIterator;

import com.sni.home.CQDataTool.config.CQDataConfig;
import com.sni.home.CQDataTool.config.CQQueryInit;
import com.sni.home.CQDataTool.utils.FileOperations;
import com.sni.home.CQDataTool.utils.GetCQProperty;
import com.sni.home.CQDataTool.utils.RecurseCQ;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CQReport {

static Logger	log	= LoggerFactory.getLogger(CQReport.class);

	public static void cqReport(CQDataConfig cfg,Query qry,String type,FileWriter fw) throws Exception {
		
		String parentnode="";
		int rowssize=0;
		int counter=0;

		log.info("Initialized the class");

		try {
			
			Query q;
			
			if(type!="image")
			{
				q = CQQueryInit.initQuery(cfg);
			}
			else
			{
				q = qry;
			}
			
			
			QueryResult result = q.execute();
			
			rowssize = (int) result.getRows().getSize();
			
			log.info("Query found " + rowssize + " rows");
			
			RowIterator rows = result.getRows();			
			
			Set<Node> childnodes= new HashSet<Node>();
			
			ArrayList<String> aggrstr = new ArrayList<String>();
			
			while (rows.hasNext()) 
			{
					Row row = rows.nextRow();
					
					Node pnode = row.getNode();
					
					parentnode = pnode.getPath().toString();

					System.out.println("Checking Node ["+counter+" of "+rowssize+"] :"+ parentnode);
					
					childnodes=RecurseCQ.recursechildren(pnode, cfg);
					
					System.out.println("After processing the following node{"+pnode.getPath().toString()+"} new array size is:"+childnodes.size());
					
					//if(WriteMultiCQProps.writemultiprops(childnodes,cfg,parentnode)){
					//	System.out.println("Child nodes found with 1 or 0 videos");
					//	fw.append(parentnode+"\n");
					//}
					
					
					aggrstr = processchildnodes(childnodes,cfg);
					
					//aggrstr = WriteMultiCQProps.writemultiprops(childnodes,cfg,parentnode);
										
					System.out.println("Total rows written to output are:"+aggrstr.size());
		
			        fw.append(parentnode);
			      
				  
				  
				  if(!type.isEmpty())
				  {
				
					  for (String outval:aggrstr)
					  {
						  //System.out.println("writing Outval:"+outval);
						  
						  fw.append(outval);
						  
						  //childnodes=null;
					  }
					  fw.append("\n");
				  }
				
				/*FileOperations.appendtoFile(cfg,aggrstr);*/
			
			childnodes.clear();
			
			counter++;
				 
			}	

			} catch (RepositoryException e) {
			// TODO Auto-generated catch block
				e.printStackTrace();
			}

	}
	
	public static ArrayList<String> processchildnodes(Set<Node> allnodes,CQDataConfig cfg) throws IOException {
		
		GetCQProperty cqp = new GetCQProperty();
		
		ArrayList<String> cpropvals = new ArrayList<String>();	
		
		int flag=0;
		
		HashMap<String,String> hmap1 = new HashMap<String,String>();
		HashMap<String,String> hmap2 = new HashMap<String,String>();		
		
			try {
				
				for(Node n:allnodes)
				{
					System.out.println("**** Gather properties and values for node:"+n.getPath().toString());
					hmap1 = cqp.getProperty(n, cfg);
					System.out.println("Size of the props hashmap is :"+hmap1.size());
					
					hmap2.putAll(hmap1);				
					
				}
				
				//System.out.println("Size of the props hashmap is :"+hmap2.size());
				
				for(String userdefinedfield:cfg.getFields())
				{
										
					for(String s:hmap2.keySet())
					{
						//System.out.println("Iterating ----"+userdefinedfield);
	
						if(userdefinedfield.equalsIgnoreCase(s))
						{
							cpropvals.add(cfg.getDelimit()+hmap2.get(s));
							flag=1;
						}
						
					}
					
					if(flag==1){
						flag=0;
					}
					else
					{
						cpropvals.add(cfg.getDelimit()+"null");
						flag=0;
					}
					
				}
				
			    } catch (Exception e) {
			    	// TODO Auto-generated catch block
			    	e.printStackTrace();
				}// End of catch block
			
			return cpropvals;
					
	}//End of corelogic


}// End CQReport