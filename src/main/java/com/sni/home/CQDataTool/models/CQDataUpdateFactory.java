package com.sni.home.CQDataTool.models;

import com.sni.home.CQDataTool.DataMigrationProject.ManageAssets;
import com.sni.home.CQDataTool.DataMigrationProject.MoveAssets;
import com.sni.home.CQDataTool.DataMigrationProject.UpdateAssetReferences;
import com.sni.home.CQDataTool.delete.BulkDeleteNodes;
import com.sni.home.CQDataTool.updates.BulkPropertyUpdate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.sni.home.CQDataTool.config.CQDataConfig;
import com.sni.home.CQDataTool.config.ExceptionMsg;
import com.sni.home.CQDataTool.interfaces.CQDataObject;
import com.sni.home.CQDataTool.updates.UpdateMultiValNode;

public class CQDataUpdateFactory  implements CQDataObject{
	
	private static final Logger log =  LoggerFactory.getLogger(CQDataUpdateFactory.class.getSimpleName());


    public void processCQObject(String beanname) {

    	ConfigurableApplicationContext context = new ClassPathXmlApplicationContext("cqdoconfig.xml");
    	CQDataConfig cqdataconfobj = (CQDataConfig) context.getBean(beanname);
    	  	

       try{

    	   if(cqdataconfobj.getUpdatetype().equalsIgnoreCase("property")) {
    		   System.out.println("Update for: "+cqdataconfobj.getUpdatetype());
    	       	   BulkPropertyUpdate.updateProperty(cqdataconfobj);
    	   }
    	   if(cqdataconfobj.getUpdatetype().equalsIgnoreCase("multproperty")) {
    		   System.out.println("Update for: "+cqdataconfobj.getUpdatetype());
    	       	   UpdateMultiValNode.updateProperty(cqdataconfobj);
    	   }
           if(cqdataconfobj.getUpdatetype().equalsIgnoreCase("updateassetreferences")) {
               System.out.println("Update for: "+cqdataconfobj.getUpdatetype());
               ManageAssets.manageAssets(cqdataconfobj);
           }
           if(cqdataconfobj.getUpdatetype().equalsIgnoreCase("bulkdeletenodes")) {
               System.out.println("Delete for: "+cqdataconfobj.getUpdatetype());
               BulkDeleteNodes.bulkDelete(cqdataconfobj);
           }

       }
        catch (Exception e)
        {
            log.error(ExceptionMsg.formatExceptionMessage("Run CQ Update Exception", e));
            context.close();
        }
        context.close();

    }


}
