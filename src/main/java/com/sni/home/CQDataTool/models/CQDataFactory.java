package com.sni.home.CQDataTool.models;

import com.sni.home.CQDataTool.interfaces.CQDataObject;

public class CQDataFactory {
	
	public static CQDataObject getCQObject(String type){
		
		if(type.equalsIgnoreCase("report")) {
			System.out.println("Calling CQDataReportFactory");
			return new CQDataReportFactory();
		}
		else if (type.equalsIgnoreCase("update")){
			System.out.println("Calling CQDataUpdateFactory");
			return new CQDataUpdateFactory();
		}
		
		return null;
		
	}

}
