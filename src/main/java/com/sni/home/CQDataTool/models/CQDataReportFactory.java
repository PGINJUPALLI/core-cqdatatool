package com.sni.home.CQDataTool.models;

import com.sni.home.CQDataTool.config.CQDataConfig;
import com.sni.home.CQDataTool.config.ExceptionMsg;
import com.sni.home.CQDataTool.interfaces.CQDataObject;
import com.sni.home.CQDataTool.reports.*;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class CQDataReportFactory implements CQDataObject {

	
	private static final Logger log =  LoggerFactory.getLogger(CQDataReportFactory.class);


    public void processCQObject(String beanname) {


    	ConfigurableApplicationContext context = new ClassPathXmlApplicationContext("cqdoconfig.xml");
    	CQDataConfig cqdataconfobj = (CQDataConfig) context.getBean(beanname);


       try{

    	  //GenericReport.cqReport(cqdataconfobj);
          AllAssetsReport.allassetsReport(cqdataconfobj);
          //TraverseAllChildren.getLeafNode(cqdataconfobj);
       }
        catch (Exception e)
        {
            log.error(ExceptionMsg.formatExceptionMessage("Run Report Exception", e));
            context.close();
        }
        context.close();

    }

}
