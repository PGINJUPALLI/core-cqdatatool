package com.sni.home.CQDataTool.DataMigrationProject;


import com.sni.home.CQDataTool.config.CQDataConfig;
import com.sni.home.CQDataTool.utils.GetJSONvalues;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jcr.*;
import java.util.ArrayList;

/**
 * Created by 161257 on 6/3/2016.
 * SELECT * FROM [nt:base] AS s where ISDESCENDANTNODE([/etc/sni-asset]) AND ((s.[sni:recipes] IS NOT NULL) OR (s.[sni:recipe] IS NOT NULL))
 */
public class UpdateAssetReferences {


    private static final Logger log = LoggerFactory.getLogger(UpdateAssetReferences.class);

    public static void updateAssetReferences(CQDataConfig cfg)throws Exception,RepositoryException {
        try {

            String[] param = null;
            String oldasset=null;
            String newasset=null;
            ArrayList<String> results;
            String varUrl=cfg.getHost()+"/bin/wcm/references.json?path=";

            param = cfg.getBufferLine().split(cfg.getDelimit(), -1);
            oldasset = param[0].toString();
            newasset = param[1].toString()+param[2].toString();
            varUrl=varUrl+oldasset;
            log.info("Parsing json for varUrl:"+varUrl);
            results= GetJSONvalues.getJSONvalues(varUrl, "pages", "references", cfg);
            if(!results.isEmpty()) {
                for (String reference : results) {
                    reference=reference.replaceAll("\"","");
                    log.info("Reference:"+reference);
                    log.info("for the above reference ,we are updating " + oldasset + " with:" + newasset);
                    updateRef(cfg.getSession().getNode(reference), oldasset, newasset, cfg);
                }
            }


    }catch (Exception e)
    {
        e.printStackTrace();
    }


}

    private static void updateRef(Node n, String oldassetlink, String newassetlink, CQDataConfig cfg) throws RepositoryException {
        String[] new_prop_vals = null;
        PropertyIterator pi = n.getProperties();

        while (pi.hasNext()) {
            Property p = pi.nextProperty();
            //Loop through properties defined in cqdoconfig.xml
            for (String lfield: cfg.getFields()) {
                if (p.getName().equalsIgnoreCase(lfield)) {
                    if (p.getDefinition().isMultiple())
                    {
                        Value[] values = p.getValues();
                        new_prop_vals = new String[values.length];
                        for (int i = 0; i < values.length; i++) {
                            String currentpropval = values[i].getString();
                            if (currentpropval.trim().toString().equalsIgnoreCase(oldassetlink)) {
                                currentpropval = currentpropval.replaceAll(currentpropval.trim().toString(), newassetlink);
                            }
                            new_prop_vals[i] = currentpropval;

                        } // End of values for loop
                        p.setValue(new_prop_vals);

                    } else {
                        if (p.getName().trim().toString().equalsIgnoreCase(lfield)) {
                            p.setValue(newassetlink);
                        }
                    }

                }

            } //End of for loop getFields

        } // End of while(pi.hasNext)
    }



}
