package com.sni.home.CQDataTool.DataMigrationProject;

/**
 * Created by 161257 on 5/31/2016.
 */

import java.util.*;
import javax.jcr.*;
import com.sni.home.CQDataTool.config.CQDataConfig;
import com.sni.home.CQDataTool.config.ExceptionMsg;
import com.sni.home.CQDataTool.move.CopyNode;
import com.sni.home.CQDataTool.move.MoveNode;
import com.sni.home.CQDataTool.updates.UpdateProperty;
import com.sni.home.CQDataTool.utils.GetCQPropertyValue;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;


public class MoveAssets {

    private static final Logger log = LoggerFactory.getLogger(MoveAssets.class);
    private static Session s;


    public static void migrateRecipe(CQDataConfig cfg) throws Exception {

        try {
            String[] arg = null;
            HashMap < String, String > ctnt_properties = new HashMap < > ();
            Set < Property > removePropArray = new HashSet < > ();
            Map < String, String > pmap = cfg.getPropertymap();
            GetCQPropertyValue cqp = new GetCQPropertyValue();
            Boolean delete_property = true;
            String destJcrStr = null;
            String[] cq_tags = null;
            Map < String, List < String > > additionalTagGrp = new HashMap < > ();
            s = cfg.getSession();


            //Split the input line and capture into variables
            try {
                arg = cfg.getBufferLine().split(cfg.getDelimit(), -1);
            } catch (Exception args_exception) {
                log.info("some thing wrong with args from input file");
                args_exception.printStackTrace();
                System.exit(0);
            }

            String srcNodeStr = arg[0].toString();
            String destinationNodeStr = arg[1].toString();
            String newnodename = arg[2].toString();
            String rmaid = arg[3].toString();
            String scrid = arg[4].toString();
            String hasscrid = arg[5].toString();


            try {
                log.info("sourcenode:" + srcNodeStr);

                // Copy the node from its location to target path
                if (CopyNode.docopy(srcNodeStr, destinationNodeStr, newnodename, cfg) == null) {
                    // if the program progress here, the copy was successful,let me acknowledge it
                    log.info("copy was successful");

                    // we are going to call jcr:content several times so lets put the concat into one variable
                    destJcrStr = destinationNodeStr + "/" + newnodename + "/jcr:content";
                    // lets resolve this node
                    Node n = s.getNode(destJcrStr);

                    // add scrid and rmaid to this new asset path
                    if (hasscrid.equalsIgnoreCase("true")) {
                        UpdateProperty.updateProperty(destJcrStr, "sni:scrid", scrid, cfg, false);
                        UpdateProperty.updateProperty(destJcrStr, "sni:rmaid", rmaid, cfg, false);
                    } else {
                        UpdateProperty.updateProperty(destJcrStr, "sni:rmaid", rmaid, cfg, false);
                    }
                    ctnt_properties.put("sni:rmaid", rmaid.toString());
                    s.save();


                    // manipulate certain properties and leave rest of them as is

                    //iterate through all the properties in newly created node
                    PropertyIterator pi = n.getProperties();


                    while (pi.hasNext()) {
                        Property p = pi.nextProperty();

                        // capture pagelink property from asset node and use it for updating content properties
                        if (p.getName().toString().equalsIgnoreCase("sni:pageLinks")) {
                            String contentNodeStr = cqp.getCQPropertyValue(p).trim();
                            ctnt_properties.put("sni:assetLink", destinationNodeStr + "/" + newnodename);
                            updateContentProperties(contentNodeStr, ctnt_properties, cfg);

                        }

                        //Special case jcr description
                        if (p.getName().toString().equalsIgnoreCase("sni:abstract")) {
                            n.setProperty("assetDescription", p.getValue());
                        }

                        //new tag structure
                        if (p.getName().toString().equalsIgnoreCase("cq:tags")) {
                            //Creating tags node
                            n.addNode("tags");
                            //Get tags property values
                            Value[] values = p.getValues();
                            //Build key value pair of unique facet and associated tags
                            for (int i = 0; i < values.length; i++) {
                                cq_tags = values[i].getString().replace("food-tags:", "").split("/", -1);
                                if (additionalTagGrp.get(cq_tags[0]) == null) {
                                    additionalTagGrp.put(cq_tags[0], new ArrayList < String > ());
                                }
                                additionalTagGrp.get(cq_tags[0]).add(values[i].getString().replace("food-tags:", ""));
                            }
                            int i = 1;
                            String[] addTags = null;
                            for (Map.Entry < String, List < String >> entry: additionalTagGrp.entrySet()) {
                                s.getNode(destJcrStr + "/tags").addNode("additionalTagGroups-" + i);
                                Node additionalTagNode = s.getNode(destJcrStr + "/tags/additionalTagGroups-" + i);
                                addTags = entry.getValue().toArray(new String[entry.getValue().size()]);
                                additionalTagNode.setProperty("additionalTagGroups", addTags);
                                i++;
                            }
                        }


                        // Keep properties defined from cqdoconfig.xml
                        for (Map.Entry < String, String > entry: pmap.entrySet()) {
                            if (entry.getKey().toString().equalsIgnoreCase(p.getName().toString()))
                            {
                                if (entry.getKey().toString().equalsIgnoreCase(entry.getValue().toString()))
                                {
                                    delete_property = false;
                                } else {
                                    delete_property = true;
                                    removePropArray.add(p);

                                    // Special Cases
                                    if (entry.getKey().toString().equalsIgnoreCase("jcr:title")) {
                                        delete_property = false;
                                    }
                                }



                                try {
                                    if (p.getDefinition().isMultiple()) {
                                        n.setProperty(entry.getValue(), p.getValues());
                                    } else {
                                        n.setProperty(entry.getValue(), p.getValue());
                                    }
                                } catch (ValueFormatException ve) {
                                    ve.printStackTrace();
                                }
                            }

                            //log.info(p.getName().toString()+"=="+entry.getKey().toString()+" delete_property:"+delete_property);
                        }

                            //log.info("************   Out of cfg entry loop, going to set delete_property of "+p.getName().toString()+" to:"+delete_property);

                        //buffer the old property for removal
                        if (delete_property) {
                            removePropArray.add(p);
                        }
                        delete_property = true;// reset delete property before iterating through next property
                    }

                    // get rid of all old properties, these are dups
                    for (Property removable_property: removePropArray) {
                        if (!removable_property.getDefinition().isProtected()) {
                            log.info("removing property " + destJcrStr + "/" + removable_property.getName().toString());
                            try {
                                n.getProperty(removable_property.getName().toString()).remove();
                            }catch(Exception e)
                            {
                                e.printStackTrace();
                            }

                        }
                    }
                    s.save();
                    if (s.nodeExists(destinationNodeStr + "/" + newnodename + "/jcr:content/nutrition")) {
                        MoveNode.domove(destinationNodeStr + "/" + newnodename + "/jcr:content/nutrition", destinationNodeStr + "/" + newnodename + "/jcr:content", "nutrition-legacy", cfg);
                    }
                    if (s.nodeExists(destinationNodeStr + "/" + newnodename + "/jcr:content/blocks")) {
                        MoveNode.domove(destinationNodeStr + "/" + newnodename + "/jcr:content/blocks", destinationNodeStr + "/" + newnodename + "/jcr:content", "blocks-legacy", cfg);
                    }

                }


            } catch (PathNotFoundException pathNotFoundException) {
                log.info("source or destination path doesnt exist" + pathNotFoundException);
            } catch (ItemExistsException pathExistsException) {
                log.info("Path: " + destinationNodeStr + "/" + newnodename + " already exists with same name in specified location");
            } catch (Exception e) {
                e.printStackTrace();
                log.info(srcNodeStr + " does not exist..skipping to next");
            }
            log.info("no more nodes found,Process Completed");
        } catch (Exception e) {
            log.error(ExceptionMsg.formatExceptionMessage(
                    "Run Update Exception", e));
        }

    }

    public static void updateContentProperties(String contentNodeStr, Map < String, String > contentProps, CQDataConfig cfg) throws RepositoryException {

        String value = null;
        for (String key: contentProps.keySet()) {
            value = contentProps.get(key).toString();
            UpdateProperty.updateProperty(contentNodeStr + "/jcr:content", key, value, cfg, true);
            s.save();
        }
    }



}