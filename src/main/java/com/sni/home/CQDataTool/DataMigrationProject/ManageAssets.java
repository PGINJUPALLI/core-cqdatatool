package com.sni.home.CQDataTool.DataMigrationProject;


import com.sni.home.CQDataTool.config.CQDataConfig;
import com.sni.home.CQDataTool.utils.FileOperations;
import com.sni.home.CQDataTool.utils.GetJSONvalues;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.net.Authenticator;
import java.net.PasswordAuthentication;
import java.util.ArrayList;

/**
 * Created by 161257 on 6/8/2016.
 */
public class ManageAssets {

    private static final Logger log = LoggerFactory.getLogger(ManageAssets.class);

    public static void manageAssets(CQDataConfig cfg) throws Exception {
        try {
            FileReader input = new FileReader(cfg.getInfile());
            BufferedReader bufRead = new BufferedReader(input);
            String myLine = null;
            int x = 1;
            int total_lines = FileOperations.countLines(cfg.getInfile());

            while ((myLine = bufRead.readLine()) != null) {
                log.info("************* Processing line#:" + x + " ," + (total_lines - x) + " more lines to go");
                log.info("Starting the process of moving assets");
                cfg.setBufferLine(myLine);
                MoveAssets.migrateRecipe(cfg);
                log.info("Finished the process of moving assets");

                log.info("Starting the process of updating moved asset references");
                cfg.setBufferLine(myLine);
                UpdateAssetReferences.updateAssetReferences(cfg);
                log.info("Finished the process of updating moved asset references");


                if (myLine == null) {
                    return;
                }
                x++;
            }
            bufRead.close();

        } catch (Exception e) {
            e.printStackTrace();
        }


    }


}