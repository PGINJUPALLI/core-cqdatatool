To run this tool
Check the Intellij_Run_Configuration.JPG in the project directory
You need to use maven for build

- The main configuration is in cqdoconfig.xml
- You need to change parameters in Bean "BASECONFIG"

**java -Xmx1G -Dinfile=C:\files\outputfiles\sampleinput.txt -Doutfile=C:\files\outputfiles\output.txt -DconfigFile=cqdoconfig.xml -jar ./target/CQDataTool-1.0.one-jar.jar**